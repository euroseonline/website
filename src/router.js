import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Register from './views/Register.vue'
import Clans from './views/Clans.vue'
import Stats from './views/Stats.vue'
import Rankings from './views/Rankings.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/stats',
      name: 'stats',
      component: Stats
    },
    {
      path: '/rankings',
      name: 'ranks',
      component: Rankings
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/clans',
      name: 'clans',
      component: Clans
    }
  ]
})