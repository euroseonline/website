const mysql = require('mysql2')
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const md5 = require('md5')
const app = express()
const port = 3000

app.use(cors());
app.options('*', cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const pool = mysql.createPool({
  host     : '188.166.126.11',
  user     : 'admin_eurose2',
  password : 'michel1',
  database : 'admin_eurose2',
});
const promisePool = pool.promise();

/* Fetchin news items for the homepage */
app.get('/', async (req, res) => {
  const [rows, fields] = await promisePool.query('SELECT * FROM news WHERE published = 1 ORDER BY date DESC')
  res.send(rows)
});

/* Fetching Clans */
app.get('/clans', async (req, res) => {
  const [clans] = await promisePool.query('SELECT name, slogan, grade, SUM(grade * cp) AS clan FROM list_clan GROUP BY name, slogan, grade ORDER BY clan DESC')
  res.send(clans)
  // console.log(clans)
});


app.get('/stats', async (req, res) => {
  const [stats] = await promisePool.query('SELECT COUNT(*) AS totalAccounts FROM accounts')
  res.send(stats[0])
  // console.log(stats[0].totalAccounts)
});

/* Get ranks (character name & level), calculated by level * exp + zulies, defined as 'ranking', exclude GM's by checking if isgm = 1. Group by charname & level so we can display those in the front-end.  Order by Ranking descending*/
app.get('/rankings', async (req, res) => {
  const [ranks] = await promisePool.query('SELECT char_name, level, SUM(level * exp + zuly) AS ranking FROM characters WHERE isgm NOT IN (SELECT isgm FROM characters WHERE isgm = 1) GROUP BY char_name, level ORDER BY ranking DESC')
  res.send(ranks)
  // console.log(ranks)
});

/*
Creating account
    Replaced .query() with .execute to escape sql injections.
    `?` inside the query is a placeholder for the given values.
    .execute() needs 2 parameters, 1 = query, 2 = values given in an array

    Added `return` before sending a response to prevent the code
    from running. (If there's an error, return the response)
*/

app.post('/createAccount', async (req, res) => {
  let checkUsername = 'SELECT username FROM accounts WHERE username= ?';
  let checkEmail = 'SELECT email FROM accounts WHERE email= ?';
  let sql = 'INSERT INTO accounts (username, password, email) VALUES (?,?,?)';

  const [usernames] = await promisePool.execute(checkUsername, [req.body.username])
  if(usernames.length >= 1) {
    return res.send({errorUsername: "Username already taken"})
  }

  const [emails] = await promisePool.execute(checkEmail, [req.body.email])
  if(emails.length >= 1) {
    return res.send({errorEmail: "Email already taken"})
  }

  const [request] = await promisePool.execute(sql, [req.body.username, md5(req.body.password), req.body.email])
  res.send({success: true})
});

app.listen(port, () => console.log(`Server running on ${port}!`));
